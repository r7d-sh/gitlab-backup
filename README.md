## Welcome to gitlab-backup

Create automatic backups and update Gitlab to the next version.

### About

If there is a need to store Gitlab backups on a form of storage other than the Gitlab instance.
This script is performing Gitlab backup sync and Giltab update.
Executed from storage machine, there are no additional files needed on the remote Gitlab machine

##### With Gitlab backups you can:

* Restore Gitlab, from local backup after your instance is broken.
* Migrate Gitlab, from local backup when moving to another instance.

### Synchronization
```
Storage Machine ->> Gitlab Instance: ssh
Storage Machine --> Gitlab Instance: gitlab-rake gitlab:backup:create
Storage Machine --> Gitlab Instance: apt-get update
Storage Machine --> Gitlab Instance: apt-get install gitlab-ee
Storage Machine ->> Gitlab Instance: rsync
Gitlab Instance --> Storage Machine: gitlab-ee_backup.tar
Gitlab Instance --> Storage Machine: gitlab_secrets.json
Gitlab Instance --> Storage Machine: gitlab.rb
```
### Files

Each script run creates a new directory structure with the backup timestamp in the name.

Example: 

* 1568970319_2019_09_20_12.2.5-ee 
    * 1568970319_2019_09_20_12.2.5-ee_gitlab_backup.tar
    * gitlab-secrets.json
    * gitlab.rb 
    * backup_log


### Requirements

* Gitlab: Any version
* Jq on storage machine.
* Configured ssh connection and rsync access on a remote machine.

### Config
Edit the backup_config.json file according to your configuration.

    "address":"192.168.0.1",                 	// Gitlab machine address
    "user": "user"                   	 		// Remote machine username      
    "pemFile": "/path/to/file.pem",             // Pem file location
    "localBackupDir": "/path/to/backup/dir"     // Local backup storage


### Usage

Run the script on your backup machine
    
    # ./backup.sh 





