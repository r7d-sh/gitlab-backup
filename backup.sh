#!/bin/bash
# Author: Daniel Podolski 
# Date: 20.09.2019
# Comment: Bash script for creating gitlab backups
# License: See LICENSE.md

set -e
parseJsonConfig () {
	echo
	echo "$(tput setaf 125)Parse config file "$config"...$(tput sgr0)"
	remoteAddress=$(jq -r .remote.address "$config")
	remoteUser=$(jq -r .remote.user "$config")
	pemfile=$(jq -r .pemFile "$config")
	localBackupDir=$(jq -r .localBackupDir "$config")
	localBackupTmpDir="/tmp/gitlab-backup/"
	localBackupTmpLog="/tmp/gitlab-backup/backup_log"
	echo "$(tput setaf 42)done.$(tput sgr0)"	
	echo
}

connectToRemote () {
	echo "$(tput setaf 125)Connect to remote machine "$remoteAddress"...$(tput sgr0)"
	ssh -t -i "$pemfile" "$remoteUser"@"$remoteAddress" 'sudo su <<\EOF
	echo "$(tput setaf 125)Clean up previous backups /var/opt/gitlab/backups/...$(tput sgr0)"
	rm -rf /var/opt/gitlab/backups/* 
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
	echo "$(tput setaf 125)Create temp directory /tmp/gitlab-backups/...$(tput sgr0)"
	if [ ! -d "/tmp/gitlab-backups/" ]; then
		mkdir /tmp/gitlab-backups/
	else
		echo "$(tput setaf 125)/tmp/gitlab-backups/ already exists, cleaning up...$(tput sgr0)"
		rm -rf /tmp/gitlab-backups/* 
	fi
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
	echo "$(tput setaf 125)Create gitlab backup...$(tput sgr0)"
	gitlab-rake gitlab:backup:create
	echo
	echo "$(tput setaf 125)Move backup /var/opt/gitlab/backups/ files to temp directory /tmp/gitlab-backups/...$(tput sgr0)"
	mv /var/opt/gitlab/backups/* /tmp/gitlab-backups/ 
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
	echo "$(tput setaf 125)Copy backup /etc/gitlab/gitlab-secrets.json to temp directory /tmp/gitlab-backups/...$(tput sgr0)"
	cp /etc/gitlab/gitlab-secrets.json /tmp/gitlab-backups/ 
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
	echo "$(tput setaf 125)Move backup /etc/gitlab/gitlab.rb files to temp directory /tmp/gitlab-backups/...$(tput sgr0)"
	cp /etc/gitlab/gitlab.rb /tmp/gitlab-backups/ 
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
	echo "$(tput setaf 125)Update and Gitlab install...$(tput sgr0)"
	sudo apt-get update && sudo apt-get install gitlab-ce
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
EOF' | tee "$localBackupTmpLog"
	echo
}

pullBackupFiles () {
	echo "$(tput setaf 125)Sync backup files clean up remote machine...$(tput sgr0)" 
	rsync -avzh --progress --stats --remove-source-files -e "ssh -i $pemfile" "$remoteUser"@"$remoteAddress":/tmp/gitlab-backups/* "$localBackupTmpDir" 
	echo "$(tput setaf 125)Extract backup timestamp$(tput sgr0)" 
	awk -F\\n '{ match($1,"([0-9]{10})_([0-9]{4})_([0-9]{2})_([0-9]{2})_([0-9]{2}).([0-9]{1}).([0-9]{1})-([a-z]{2})");print substr($1,RSTART,RLENGTH),$2,$3}' "$localBackupTmpDir"/backup_log > "$localBackupTmpDir"/backup_name
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo 
}
 
organizeBackupDirectory () {
	echo "$(tput setaf 125)Sync to local backup directory...$(tput sgr0)"
	backupFileName=$(tr -d " \t\n\r" < "$localBackupTmpDir"/backup_name ) 

	echo "$backupFileName"

	currentBackupDir="$(localBackupDir)$(backupFileName)"
	echo "$(tput setaf 125)Create backup directory "$currentBackupDir"...$(tput sgr0)" 
	echo "$currentBackupDir"

	mkdir "$currentBackupDir"

	rm "$localBackupTmpDir"/backup_name

	rsync -avzh "$localBackupTmpDir" "$currentBackupDir"
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
}

cleanUpLocalMachine () {
	echo "$(tput setaf 125)Clean up local machine "$localBackupTmpDir"...$(tput sgr0)" 
	rm -rf "$localBackupTmpDir"
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo
}

main () 
{
	currDirName=$(dirname "$0")
	config="$currDirName/backup_config.json"

	if [ ! -f "$config" ]; then
	    echo "$config not found!"
	    exit 1
	fi

	parseJsonConfig

	echo "$(tput setaf 125)Create temp dir "$localBackupTmpDir"...$(tput sgr0)"

	if [ ! -d "/tmp/gitlab-backups/" ]; then
		mkdir "$localBackupTmpDir"
		touch "$localBackupTmpLog"
	else
		echo "$(tput setaf 125)/tmp/gitlab-backups/ already exists, cleaning up...$(tput sgr0)"
		rm -rf /tmp/gitlab-backups/* 
	fi
	echo "$(tput setaf 42)done.$(tput sgr0)"
	echo

	connectToRemote

	pullBackupFiles

	organizeBackupDirectory

	cleanUpLocalMachine
}

main